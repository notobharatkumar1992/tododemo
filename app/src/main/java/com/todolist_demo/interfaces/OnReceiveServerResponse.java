package com.todolist_demo.interfaces;

public interface OnReceiveServerResponse {

    public void setOnReceiveResult(String apiName, String result);

}
