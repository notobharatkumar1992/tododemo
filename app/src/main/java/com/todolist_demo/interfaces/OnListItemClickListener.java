package com.todolist_demo.interfaces;

/**
 * Created by bharat on 26/12/15.
 */
public interface OnListItemClickListener {

    public void setOnListItemClickListener(String name, int position);

    public void setOnListItemClickListener(String name, String id, int position);
}
