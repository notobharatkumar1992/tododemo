package com.todolist_demo;

import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.todolist_demo.Utils.SpacesItemDecoration;
import com.todolist_demo.adaper.ToDoRecyclerViewAdapter;
import com.todolist_demo.async.PostAsync;
import com.todolist_demo.constants.ServerRequestConstants;
import com.todolist_demo.constants.Tags;
import com.todolist_demo.interfaces.OnListItemClickListener;
import com.todolist_demo.interfaces.OnReceiveServerResponse;
import com.todolist_demo.model.DemoModel;
import com.todolist_demo.model.PostAysnc_Model;
import com.todolist_demo.parser.JSONParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements OnReceiveServerResponse, View.OnClickListener, OnListItemClickListener {

    public Handler mHandler;
    private ImageView img_right;
    private TextView txt_header, txt_pending, txt_done;
    private ArrayList<DemoModel> doneTaskList = new ArrayList<>();
    public ArrayList<DemoModel> pendingTaskList = new ArrayList<>();
    private int selected_tab = 0;

    private RecyclerView recyclerView;
    private ToDoRecyclerViewAdapter toDoPendingRecyclerViewAdapter, toDoDoneRecyclerViewAdapter;
    SwipyRefreshLayout swipyrefreshlayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        setHandler();
        callAsync(0);
    }

    private void initView() {
        img_right = (ImageView) findViewById(R.id.img_right);
        img_right.setOnClickListener(this);
        txt_header = (TextView) findViewById(R.id.txt_header);
        txt_header.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Lato)));
        txt_pending = (TextView) findViewById(R.id.txt_pending);
        txt_pending.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Lato)));
        txt_pending.setOnClickListener(this);
        txt_pending.setSelected(true);
        txt_done = (TextView) findViewById(R.id.txt_done);
        txt_done.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Lato)));
        txt_done.setOnClickListener(this);
        txt_done.setSelected(false);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setPadding(AppDelegate.dpToPix(MainActivity.this, 5), AppDelegate.dpToPix(MainActivity.this, 5), AppDelegate.dpToPix(MainActivity.this, 5), AppDelegate.dpToPix(MainActivity.this, 5));
        recyclerView.addItemDecoration(new SpacesItemDecoration(AppDelegate.dpToPix(MainActivity.this, 5), true));

        swipyrefreshlayout = (SwipyRefreshLayout) findViewById(R.id.swipyrefreshlayout);
        swipyrefreshlayout.setDirection(SwipyRefreshLayoutDirection.TOP);
        swipyrefreshlayout.setOnRefreshListener
                (new SwipyRefreshLayout.OnRefreshListener() {
                     @Override
                     public void onRefresh(SwipyRefreshLayoutDirection direction) {
                         if (direction == SwipyRefreshLayoutDirection.TOP && AppDelegate.haveNetworkConnection(MainActivity.this)) {
                             callAsync(1);
                         } else {
                             swipyrefreshlayout.setRefreshing(false);
                         }
                     }
                 }
                );
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_pending:
                img_right.setVisibility(View.VISIBLE);
                txt_pending.setSelected(true);
                txt_done.setSelected(false);
                selected_tab = 0;
                refreshList(0);
                break;
            case R.id.txt_done:
                img_right.setVisibility(View.GONE);
                txt_done.setSelected(true);
                txt_pending.setSelected(false);
                selected_tab = 1;
                refreshList(1);
                break;
            case R.id.img_right:
                if (longClickEnable) {
                    longClickEnable = false;
                    for (int i = pendingTaskList.size() - 1; 0 <= i; i--) {
                        AppDelegate.LogT("Items = " + i);
                        if (pendingTaskList.get(i).selected == 1) {
                            AppDelegate.LogT("removed = " + i);
                            pendingTaskList.remove(i);
                        }
                    }
                    img_right.setImageResource(R.drawable.add);
                    mHandler.sendEmptyMessage(1);
                } else {
                    showAlertDialog();
                }
                break;
        }
    }

    public void showAlertDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);

        final EditText input = new EditText(MainActivity.this);
        input.setHint("Enter title here");
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        alertDialog.setView(input);
        alertDialog.setTitle("To Do");

        alertDialog.setPositiveButton("YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (AppDelegate.isValidString(input.getText().toString())) {
                            dialog.cancel();
                            DemoModel demoModel = new DemoModel();
                            demoModel.id = "de" + pendingTaskList.size() + 1;
                            demoModel.name = input.getText().toString();
                            pendingTaskList.add(demoModel);
                            mHandler.sendEmptyMessage(1);
                        } else {
                            AppDelegate.showToast(MainActivity.this, "Please enter title.");
                        }
                    }
                });

        alertDialog.setNegativeButton("NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        alertDialog.show();
    }


    private void refreshList(int value) {
        if (value == 0) {
            toDoPendingRecyclerViewAdapter = new ToDoRecyclerViewAdapter(this, pendingTaskList, this);
            recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
            recyclerView.setAdapter(toDoPendingRecyclerViewAdapter);
        } else {
            toDoDoneRecyclerViewAdapter = new ToDoRecyclerViewAdapter(this, doneTaskList, this);
            recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
            recyclerView.setAdapter(toDoDoneRecyclerViewAdapter);
        }
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(MainActivity.this);
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(MainActivity.this);
                } else if (msg.what == 1) {
                    refreshList(selected_tab);
                }
            }
        };
    }

    public void callAsync(int type) {
        if (AppDelegate.haveNetworkConnection(this)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<>();
            PostAsync mPostAsyncObj = new PostAsync(this,
                    this, ServerRequestConstants.BASE_URL,
                    mPostArrayList, null);
            if (type == 0) {
                mHandler.sendEmptyMessage(10);
            }
            mPostAsyncObj.execute();
        }
    }

    @Override
    public void setOnReceiveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(11);
        swipyrefreshlayout.setRefreshing(false);
        if (apiName.equalsIgnoreCase(ServerRequestConstants.BASE_URL)) {
            parseResponse(result);
        }
    }

    private void parseResponse(String result) {
        try {
            if (AppDelegate.isValidString(result)) {
                JSONObject object = new JSONObject(result);
                JSONArray jsonArray = object.getJSONArray(Tags.DATA);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    DemoModel demoModel = new DemoModel();
                    demoModel.id = JSONParser.getString(jsonObject, Tags.id);
                    demoModel.name = JSONParser.getString(jsonObject, Tags.name);
                    demoModel.state = JSONParser.getString(jsonObject, Tags.state);
                    if (demoModel.state.equalsIgnoreCase("0")) {
                        if (pendingTaskList.size() == 0) {
                            pendingTaskList.add(demoModel);
                        } else {
                            boolean isPresentValue = false;
                            for (int j = 0; j < pendingTaskList.size(); j++) {
                                if (pendingTaskList.get(j).id.equalsIgnoreCase(demoModel.id)) {
                                    isPresentValue = true;
                                    pendingTaskList.remove(j);
                                    pendingTaskList.add(j, demoModel);
                                    break;
                                }
                            }
                            if (!isPresentValue) {
                                pendingTaskList.add(demoModel);
                            }
                        }
                    } else {
                        if (doneTaskList.size() == 0) {
                            doneTaskList.add(demoModel);
                        } else {
                            boolean isPresentValue = false;
                            for (int j = 0; j < doneTaskList.size(); j++) {
                                if (doneTaskList.get(j).id.equalsIgnoreCase(demoModel.id)) {
                                    isPresentValue = true;
                                    doneTaskList.remove(j);
                                    doneTaskList.add(j, demoModel);
                                    break;
                                }
                            }
                            if (!isPresentValue) {
                                doneTaskList.add(demoModel);
                            }
                        }
                    }
                }
                mHandler.sendEmptyMessage(1);
            } else {
                AppDelegate.showToast(this, "Please try again later");
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    public boolean longClickEnable = false;

    @Override
    public void setOnListItemClickListener(String name, int position) {
        if (name.equalsIgnoreCase(Tags.LONG_CLICK)) {
            img_right.setImageResource(R.drawable.delete);
            if (selected_tab == 0) {
                longClickEnable = true;
                pendingTaskList.get(position).selected = pendingTaskList.get(position).selected == 0 ? 1 : 0;
                mHandler.sendEmptyMessage(1);
            }

        } else if (name.equalsIgnoreCase(Tags.clicks)) {
            if (longClickEnable) {
                if (selected_tab == 0) {
                    pendingTaskList.get(position).selected = pendingTaskList.get(position).selected == 0 ? 1 : 0;
                    mHandler.sendEmptyMessage(1);
                }

            } else {
                if (selected_tab == 0) {
                    for (int i = 0; i < pendingTaskList.size(); i++) {
                        pendingTaskList.get(i).expiry_date = "";
                    }
                    pendingTaskList.get(position).expiry_date = "2000";
                } else {
                    for (int i = 0; i < doneTaskList.size(); i++) {
                        doneTaskList.get(i).expiry_date = "";
                    }
                    doneTaskList.get(position).expiry_date = "2000";
                }
                mHandler.sendEmptyMessage(1);
            }

        } else if (name.equalsIgnoreCase(Tags.stop)) {
            if (selected_tab == 0) {
                pendingTaskList.get(position).expiry_date = "";
            } else {
                doneTaskList.get(position).expiry_date = "";
            }
            mHandler.sendEmptyMessage(1);

        }
    }

    @Override
    public void setOnListItemClickListener(String name, String id, int position) {
        if (name.equalsIgnoreCase(Tags.timeRange)) {
            if (selected_tab == 0) {
                int value = -1;
                for (int i = 0; i < pendingTaskList.size(); i++) {
                    if (id.equalsIgnoreCase(pendingTaskList.get(i).id) && AppDelegate.isValidString(pendingTaskList.get(i).expiry_date)) {
                        value = i;
                        break;
                    }
                }
                if (value == -1) {
                    return;
                }
                pendingTaskList.get(value).expiry_date = "";
                pendingTaskList.get(value).state = "1";
                doneTaskList.add(pendingTaskList.get(value));
                pendingTaskList.remove(value);
            } else {
                int value = -1;
                for (int i = 0; i < doneTaskList.size(); i++) {
                    if (id.equalsIgnoreCase(doneTaskList.get(i).id)  && AppDelegate.isValidString(doneTaskList.get(i).expiry_date)) {
                        value = i;
                        break;
                    }
                }
                if (value == -1) {
                    return;
                }
                doneTaskList.get(position).expiry_date = "";
                doneTaskList.get(position).state = "0";
                pendingTaskList.add(doneTaskList.get(position));
                doneTaskList.remove(position);
            }
            mHandler.sendEmptyMessage(1);
        }
    }
}
