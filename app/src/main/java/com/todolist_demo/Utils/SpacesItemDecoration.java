package com.todolist_demo.Utils;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by bharat on 24/7/16.
 */
public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
    private int space, count = 0;
    private boolean value = false;

    public SpacesItemDecoration(int space) {
        this.space = space;
    }

    public SpacesItemDecoration(int space, boolean value) {
        this.space = space;
        this.value = value;
    }

    public SpacesItemDecoration(int space, int count) {
        this.space = space;
        this.count = count;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.left = 0;
        outRect.right = 0;
        outRect.bottom = space;
        outRect.top = 0;
    }
}