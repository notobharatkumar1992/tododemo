package com.todolist_demo.adaper;

import android.os.CountDownTimer;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.todolist_demo.R;


public class ToDoViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

    TextView txt_data, txt_cancel;
    RelativeLayout rl_main;
    CountDownTimer countDownTimer;

    public ToDoViewHolders(View convertView) {
        super(convertView);
        convertView.setOnClickListener(this);
        txt_data = (TextView) convertView.findViewById(R.id.txt_data);
        txt_cancel = (TextView) convertView.findViewById(R.id.txt_cancel);
        rl_main = (RelativeLayout) convertView.findViewById(R.id.rl_main);
    }

    @Override
    public void onClick(View view) {
        Toast.makeText(view.getContext(), "Clicked Position = " + getPosition(), Toast.LENGTH_SHORT).show();

    }
}
