package com.todolist_demo.adaper;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.CountDownTimer;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.todolist_demo.AppDelegate;
import com.todolist_demo.R;
import com.todolist_demo.constants.Tags;
import com.todolist_demo.interfaces.OnListItemClickListener;
import com.todolist_demo.model.DemoModel;

import java.util.ArrayList;

public class ToDoRecyclerViewAdapter extends RecyclerView.Adapter<ToDoViewHolders> {

    private ArrayList<DemoModel> dealArray;
    private Context mContext;
    private OnListItemClickListener itemClickListener;

    public ToDoRecyclerViewAdapter(Context mContext, ArrayList<DemoModel> dealArray, OnListItemClickListener itemClickListener) {
        this.dealArray = dealArray;
        this.mContext = mContext;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public ToDoViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.do_item, null);
        ToDoViewHolders rcv = new ToDoViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final ToDoViewHolders holder, final int position) {
        try {
            holder.txt_data.setTypeface(Typeface.createFromAsset(mContext.getAssets(), mContext.getResources().getString(R.string.font_Lato)));
            holder.txt_cancel.setTypeface(Typeface.createFromAsset(mContext.getAssets(), mContext.getResources().getString(R.string.font_Lato)));

            holder.txt_data.setText(dealArray.get(position).name);
            if (dealArray.get(position).selected == 1) {
                holder.rl_main.setBackgroundColor(mContext.getResources().getColor(R.color.grey_font));
            } else {
                holder.rl_main.setBackgroundColor(Color.WHITE);
            }

            holder.rl_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemClickListener != null)
                        itemClickListener.setOnListItemClickListener(Tags.clicks, position);
                }
            });
            holder.rl_main.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (itemClickListener != null)
                        itemClickListener.setOnListItemClickListener(Tags.LONG_CLICK, position);
                    return false;
                }
            });
            holder.txt_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemClickListener != null)
                        itemClickListener.setOnListItemClickListener(Tags.stop, position);
                    if (holder.countDownTimer != null) {
                        holder.countDownTimer.cancel();
                    }
                }
            });
            if (AppDelegate.isValidString(dealArray.get(position).expiry_date)) {
                if (holder.countDownTimer != null) {
                    holder.countDownTimer.cancel();
                }
                holder.txt_cancel.setVisibility(View.VISIBLE);
                holder.countDownTimer = new CountDownTimer(2000, 1000) { // adjust the milli seconds here
                    public void onTick(long millisUntilFinished) {
                        AppDelegate.LogT("position => " + position);
                    }

                    public void onFinish() {
                        holder.txt_cancel.setVisibility(View.GONE);
                        if (itemClickListener != null)
                            itemClickListener.setOnListItemClickListener(Tags.timeRange, dealArray.get(position).id, position);
//                    holder.txt_c_timer.setText("Coupon expired");
                    }
                }.start();
            } else if (holder.countDownTimer != null) {
                holder.countDownTimer.cancel();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }

    }


    @Override
    public int getItemCount() {
        return this.dealArray.size();
    }
}
