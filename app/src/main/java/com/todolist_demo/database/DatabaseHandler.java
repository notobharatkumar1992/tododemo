package com.todolist_demo.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Bharat on 06/02/2016.
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    public DatabaseHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

//    // All Static variables
//    // Database Version
//    private static final int DATABASE_VERSION = 1;
//
//    // Database Name
//    private static final String DATABASE_NAME = "stux";
//
//    // Contacts table name
//    private static final String TABLE_SLIDERS = "sliders";
//
//    // Contacts Table Columns names
//    private static final String KEY_ID = "id";
//    private static final String KEY_NAME = "name";
//    private static final String KEY_PH_NO = "phone_number";
//    private static final String KEY_IMG = "img";
//    private static final String KEY_BANNER_THUMB = "banner_thumb_image";
//    private static final String KEY_SLIDER_CATEGORY = "slider_category";
//    private static final String KEY_EXPIRY_DATE = "expiry_date";
//    private static final String KEY_STATUS = "status";
//    private static final String KEY_SINGLE_CLICKED = "single_clicked";
//    private static final String KEY_THUMB_CLICKED = "thump_clicked";
//
//
//    public DatabaseHandler(Context context) {
//        super(context, DATABASE_NAME, null, DATABASE_VERSION);
//    }
//
//    // Creating Tables
//    @Override
//    public void onCreate(SQLiteDatabase db) {
//        String CREATE_SLIDER_TABLE = "CREATE TABLE " + TABLE_SLIDERS + "("
//                + KEY_ID + " INTEGER PRIMARY KEY,"
//                + KEY_IMG + " TEXT,"
//                + KEY_BANNER_THUMB + " TEXT,"
//                + KEY_SLIDER_CATEGORY + " TEXT,"
//                + KEY_EXPIRY_DATE + " TEXT,"
//                + KEY_STATUS + " TEXT,"
//                + KEY_SINGLE_CLICKED + " TEXT,"
//                + KEY_THUMB_CLICKED + " TEXT"
//                + ")";
//        db.execSQL(CREATE_SLIDER_TABLE);
//    }
//
//    // Upgrading database
//    @Override
//    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//        // Drop older table if existed
//        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SLIDERS);
//        // Create tables again
//        onCreate(db);
//    }
//
//    // Adding new slider
//    public void addSliderModel(SliderModel sliderModel) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        ContentValues values = new ContentValues();
//        values.put(KEY_ID, sliderModel.id);
//        values.put(KEY_IMG, sliderModel.banner_image);
//        values.put(KEY_BANNER_THUMB, sliderModel.banner_thumb_image);
//        values.put(KEY_SLIDER_CATEGORY, sliderModel.slider_category);
//        values.put(KEY_EXPIRY_DATE, sliderModel.expiry_date);
//        values.put(KEY_STATUS, sliderModel.status);
//        values.put(KEY_SINGLE_CLICKED, sliderModel.single_clicked);
//        values.put(KEY_THUMB_CLICKED, sliderModel.thump_clicked);
//
//        // Inserting Row
//        db.insert(TABLE_SLIDERS, null, values);
//        db.close(); // Closing database connection
//    }
//
//    // Getting single slider
//    public SliderModel getSliderModel(String id) {
//        SQLiteDatabase db = this.getReadableDatabase();
//
//        Cursor cursor = db.query(TABLE_SLIDERS, new String[]{KEY_ID,
//                        KEY_NAME, KEY_PH_NO}, KEY_ID + "=?",
//                new String[]{id}, null, null, null, null);
//
//        SliderModel sliderModel = null;
//        if (cursor != null) {
//            cursor.moveToFirst();
//
//            sliderModel = new SliderModel();
//            sliderModel.id = cursor.getString(0);
//            sliderModel.banner_image = cursor.getString(1);
//            sliderModel.banner_thumb_image = cursor.getString(2);
//            sliderModel.slider_category = cursor.getString(3);
//            sliderModel.expiry_date = cursor.getString(4);
//            sliderModel.status = cursor.getString(5);
//            sliderModel.single_clicked = Integer.parseInt(cursor.getString(6));
//            sliderModel.thump_clicked = Integer.parseInt(cursor.getString(7));
//        }
//        // return slider
//        return sliderModel;
//    }
//
//    // Getting All slider
//    public List<SliderModel> getAllSlidersModel() {
//        List<SliderModel> sliderList = new ArrayList<SliderModel>();
//        // Select All Query
//        String selectQuery = "SELECT  * FROM " + TABLE_SLIDERS;
//
//        SQLiteDatabase db = this.getWritableDatabase();
//        Cursor cursor = db.rawQuery(selectQuery, null);
//
//        // looping through all rows and adding to list
//        if (cursor.moveToFirst()) {
//            do {
//                SliderModel sliderModel = new SliderModel();
//                sliderModel.id = cursor.getString(0);
//                sliderModel.banner_image = cursor.getString(1);
//                sliderModel.banner_thumb_image = cursor.getString(2);
//                sliderModel.slider_category = cursor.getString(3);
//                sliderModel.expiry_date = cursor.getString(4);
//                sliderModel.status = cursor.getString(5);
//                sliderModel.single_clicked = Integer.parseInt(cursor.getString(6));
//                sliderModel.thump_clicked = Integer.parseInt(cursor.getString(7));
//                // Adding contact to list
//                sliderList.add(sliderModel);
//            } while (cursor.moveToNext());
//        }
//
//        // return slider list
//        return sliderList;
//    }
//
//    // Getting slider Count
//    public int getSlidersCount() {
//        String countQuery = "SELECT  * FROM " + TABLE_SLIDERS;
//        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor cursor = db.rawQuery(countQuery, null);
//        cursor.close();
//
//        // return count
//        return cursor.getCount();
//    }
//
//    // Updating single slider
//    public int updateSlider(SliderModel sliderModel) {
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        ContentValues values = new ContentValues();
//        values.put(KEY_ID, sliderModel.id);
//        values.put(KEY_IMG, sliderModel.banner_image);
//        values.put(KEY_BANNER_THUMB, sliderModel.banner_thumb_image);
//        values.put(KEY_SLIDER_CATEGORY, sliderModel.slider_category);
//        values.put(KEY_EXPIRY_DATE, sliderModel.expiry_date);
//        values.put(KEY_STATUS, sliderModel.status);
//        values.put(KEY_SINGLE_CLICKED, sliderModel.single_clicked);
//        values.put(KEY_THUMB_CLICKED, sliderModel.thump_clicked);
//
//        // updating row
//        return db.update(TABLE_SLIDERS, values, KEY_ID + " = ?", new String[]{sliderModel.id});
//    }
//
//    // Deleting single slider
//    public void deleteSlider(SliderModel sliderModel) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        db.delete(TABLE_SLIDERS, KEY_ID + " = ?", new String[]{sliderModel.id});
//        db.close();
//    }
}
